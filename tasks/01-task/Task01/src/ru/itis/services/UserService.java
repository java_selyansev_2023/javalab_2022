package ru.itis.services;

import ru.itis.dto.SignUpForm;

public interface UserService {

    void signUp(SignUpForm signUpForm);

}
