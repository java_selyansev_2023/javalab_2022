update client
    set phone_number = '83333333333'
where id = 4;

update driver
    set cars_name = 'Range Rover'
where id = 1;

update car
    set release_year = 2022
where id = 1;

update ord
    set ord_number = 20
where price = 200;

insert into client(first_name, last_name, age, phone_number)
values ('Марсель', 'Сидиков', 28, '89999999999');
insert into client(first_name, last_name, age, phone_number)
values ('Камила', 'Дмитриева', 19, '87777777777');
insert into client(first_name, last_name, age, phone_number)
values ('Илья', 'Казначеев', 18, '86666666666');
insert into client(first_name, last_name, age, phone_number)
values ('Влад', 'Селянцев', 18, '85555555555');

insert into driver (first_name, last_name, age, cars_name)
values ('Марсель', 'Сидиков', 28, 'Kia rio');

insert into car (car_name, release_year, serial_number)
values ('Tesla Plaid', 2020, '40395134028105927');
insert into car (car_name, release_year, serial_number)
values ('Lada Kalina', 2010, '54234039513402927');


insert into ord (ord_number, price)
values (10, 200)