drop table if exists client;
drop table if exists driver;
drop table if exists car;
drop table if exists ord;

create table client (
   id           bigserial primary key,
   first_name   char(20),
   last_name    char(20),
   age          integer check ( age > 10 and age < 65 )
);

create table driver (
    id          bigserial primary key,
    first_name  char(20),
    last_name   char(20),
    age         integer check ( age > 21 ),
    cars_name   char(30)
);

create table car (
    id          bigserial primary key,
    car_name    char(30),
    release_year integer check ( release_year > 2000 )
);

create table ord(
    id          bigserial primary key,
    ord_number  integer check ( ord_number > 0 )
);

alter table driver
    add phone_number char(11) not null default '';

alter table client
    add phone_number char(11) not null default '';

alter table car
    add serial_number char(17) not null default '';

alter table ord
    add price integer check ( price >= 60 );