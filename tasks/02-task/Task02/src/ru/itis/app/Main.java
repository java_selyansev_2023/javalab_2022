package ru.itis.app;

import ru.itis.dto.SignUpForm;
import ru.itis.mappers.Mappers;
import ru.itis.models.User;
import ru.itis.repositories.UsersRepository;
import ru.itis.repositories.UsersRepositoryFilesImpl;
import ru.itis.services.UserService;
import ru.itis.services.UserServiceImpl;

import java.util.UUID;

public class Main {

    public static void main(String[] args) {

        UsersRepository usersRepository = new UsersRepositoryFilesImpl("C:/Users/Vladislav/Desktop/javalab_2022/tasks/01-task/Task01/user.txt");
        UserService userService = new UserServiceImpl(usersRepository, Mappers::fromSignUpForm);

        userService.signUp(new SignUpForm(
                "Марсель",
                "Сидиков",
                "sidikov@gmail.com",
                "qwert007")
        );


        System.out.println(usersRepository.findAll());

        User user = new User(
                UUID.randomUUID(),
                "Vlad",
                "Selyantsev",
                "vlad@gmail.com",
                "qwert001"
        );

        usersRepository.update(user);

        usersRepository.delete(user);

        usersRepository.deleteById(UUID.fromString("adb3218d-5042-4dfa-966e-21d4ff530b37"));

        System.out.println(usersRepository.findById(UUID.fromString("f77eecb0-55af-4874-b561-95c7cfa41512")));
    }
}
