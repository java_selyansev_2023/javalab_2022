package ru.itis.mappers;

import ru.itis.dto.SignUpForm;
import ru.itis.models.User;

public class Mappers {

    public static User fromSignUpForm(SignUpForm form) {
        return new User(
                form.getFirstName(),
                form.getLastName(),
                form.getEmail(),
                form.getPassword()
        );
    }

}
