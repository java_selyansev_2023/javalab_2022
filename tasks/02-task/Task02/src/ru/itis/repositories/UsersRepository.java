package ru.itis.repositories;

import ru.itis.models.User;

import java.util.UUID;

public interface UsersRepository extends CrudRepository<UUID, User> {

}
