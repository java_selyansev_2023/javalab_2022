package ru.itis.repositories;

import ru.itis.models.User;

import javax.sql.rowset.WebRowSet;
import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.UUID;
import java.util.function.Function;

public class UsersRepositoryFilesImpl implements UsersRepository {

    private final String fileName;
    private UUID id;

    public UsersRepositoryFilesImpl(String fileName) {
        this.fileName = fileName;
        this.id = id;
    }

    private static Function<User, String> userToString = user ->
            user.getId()
                    + "|" + user.getFirstName()
                    + "|" + user.getLastName()
                    + "|" + user.getEmail()
                    + "|" + user.getPassword();

    @Override
    public List<User> findAll() {

        List<User> allUsersResult = new ArrayList<>();

        try (Scanner scanner = new Scanner(new File(fileName))) {

            while (scanner.hasNextLine()) {
                String temp = scanner.nextLine();
                String[] allUsersArray = temp.split("\\|");

                User user = new User(
                        UUID.fromString(allUsersArray[0]),
                        allUsersArray[1],
                        allUsersArray[2],
                        allUsersArray[3],
                        allUsersArray[4]
                );

                allUsersResult.add(user);
            }
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }

        return allUsersResult;
    }

    @Override
    public void save(User entity) {
        id = UUID.randomUUID();
        entity.setId(id);

        try (Writer writer = new FileWriter(fileName, true);
             BufferedWriter bufferedWriter = new BufferedWriter(writer)) {

            String userAsString = userToString.apply(entity);
            bufferedWriter.write(userAsString);
            bufferedWriter.newLine();

        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }

    }

    @Override
    public void update(User entity) {

        try (BufferedReader br = new BufferedReader(new FileReader(fileName));
            BufferedWriter bw = new BufferedWriter(new FileWriter("fileAfterDeleting"))) {

            while (br.readLine() != null) {
                String temp = br.readLine();
                String[] tempArray = temp.split("\\|");

                if (entity.getId().equals(UUID.fromString(tempArray[0]))) {
                    String userAsString = userToString.apply(entity);

                    bw.write(userAsString);
                } else {
                    bw.write(temp);
                }
            }

        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }

    }

    @Override
    public void delete(User entity) {

        try (BufferedReader br = new BufferedReader(new FileReader(fileName));
             BufferedWriter bw = new BufferedWriter(new FileWriter("fileAfterDeleting"))) {

            while (br.readLine() != null) {
                String temp = br.readLine();
                String[] tempArray = temp.split("\\|");

                if (!entity.getId().equals(UUID.fromString(tempArray[0]))) {
                    bw.write(temp);
                } else {
                    bw.write("Successful Deletion!");
                }
            }

        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }
    }

    @Override
    public void deleteById(UUID uuid) {

        try (BufferedReader br = new BufferedReader(new FileReader(fileName));
             BufferedWriter bw = new BufferedWriter(new FileWriter("fileAfterDeletingByID"))) {

            while (br.readLine() != null) {
                String temp = br.readLine();
                String[] tempArray = temp.split("\\|");

                if (uuid.equals(UUID.fromString(tempArray[0]))) {
                    bw.write(temp);
                } else {
                    bw.write("Successful Deletion By ID!");
                }
            }

        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }
    }

    @Override
    public User findById(UUID uuid) {

        try (BufferedReader br = new BufferedReader(new FileReader(fileName))) {

            while (br.readLine() != null) {
                String temp = br.readLine();
                String[] tempArray = temp.split("\\|");

                if (uuid.equals(UUID.fromString(tempArray[0]))) {
                    return new User(
                            UUID.fromString(tempArray[0]),
                            tempArray[1],
                            tempArray[2],
                            tempArray[3],
                            tempArray[4]
                    );
                }
            }
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }

        return null;
    }
}
