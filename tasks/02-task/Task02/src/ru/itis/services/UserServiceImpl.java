package ru.itis.services;

import ru.itis.dto.SignUpForm;
import ru.itis.models.User;
import ru.itis.repositories.UsersRepository;

import java.util.function.Function;

public class UserServiceImpl implements UserService {

    public final UsersRepository usersRepository;
    public final Function<SignUpForm, User> toUserMapper;

    public UserServiceImpl(UsersRepository usersRepository, Function<SignUpForm, User> toUserMapper) {
        this.usersRepository = usersRepository;
        this.toUserMapper = toUserMapper;
    }

    @Override
    public void signUp(SignUpForm signUpForm) {
        User user = toUserMapper.apply(signUpForm);

        usersRepository.save(user);
    }
}
