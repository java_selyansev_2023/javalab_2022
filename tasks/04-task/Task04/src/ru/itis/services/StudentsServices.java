package ru.itis.services;

import ru.itis.dto.StudentSignUp;

public interface StudentsServices {

    void signUp(StudentSignUp form);

}
