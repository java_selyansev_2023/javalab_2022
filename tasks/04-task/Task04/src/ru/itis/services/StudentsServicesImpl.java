package ru.itis.services;

import ru.itis.dto.StudentSignUp;
import ru.itis.models.Student;
import ru.itis.repositories.StudentRepository;

public class StudentsServicesImpl implements StudentsServices {

    private final StudentRepository studentRepository;

    public StudentsServicesImpl(StudentRepository studentRepository) {
        this.studentRepository = studentRepository;
    }



    @Override
    public void signUp(StudentSignUp form) {

        Student student = new Student(
                form.getFirstName(),
                form.getLastName(),
                form.getEmail(),
                form.getPassword()
        );

        studentRepository.save(student);

    }
}
