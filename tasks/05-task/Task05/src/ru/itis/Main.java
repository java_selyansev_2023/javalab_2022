package ru.itis;

import ru.itis.dto.StudentSignUp;
import ru.itis.jdbc.SimpleDataSource;
import ru.itis.models.Student;
import ru.itis.repositories.StudentRepository;
import ru.itis.repositories.StudentRepositoryJdbcImpl;
import ru.itis.services.StudentsServices;
import ru.itis.services.StudentsServicesImpl;

import javax.sql.DataSource;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.sql.*;
import java.util.Properties;

public class Main {


    public static void main(String[] args) {
        Properties properties = new Properties();
        try {
            properties.load(Files.newInputStream(Paths.get("Task04/resources/db.properties")));
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }

        DataSource dataSource = new SimpleDataSource(
                properties.getProperty("db.url"),
                properties.getProperty("db.username"),
                properties.getProperty("db.password")
        );

        StudentRepository studentsRepository = new StudentRepositoryJdbcImpl(dataSource);
        StudentsServices studentsService = new StudentsServicesImpl(studentsRepository);

        Student student = studentsRepository.findById(1L).orElseThrow(IllegalArgumentException::new);

        Student student1 = studentsRepository.findById(4L).orElseThrow(IllegalArgumentException::new);
        System.out.println(student);
        studentsRepository.update(student1);

        studentsRepository.delete(4L);
    }
}

