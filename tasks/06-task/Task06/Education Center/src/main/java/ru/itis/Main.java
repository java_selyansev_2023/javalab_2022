package ru.itis;

import ru.itis.jdbc.SimpleDataSource;
import ru.itis.models.Student;
import ru.itis.repositories.StudentRepository;
import ru.itis.repositories.StudentRepositoryJdbcTemplateImpl;
import ru.itis.services.StudentsServices;
import ru.itis.services.StudentsServicesImpl;

import javax.sql.DataSource;
import java.io.IOException;
import java.sql.SQLOutput;
import java.util.Properties;

public class Main {


    public static void main(String[] args) {
        Properties properties = new Properties();
        try {
            properties.load(Main.class.getResourceAsStream("/db.properties"));
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }

        DataSource dataSource = new SimpleDataSource(
                properties.getProperty("db.url"),
                properties.getProperty("db.username"),
                properties.getProperty("db.password")
        );

        StudentRepository studentsRepository = new StudentRepositoryJdbcTemplateImpl(dataSource);

        Student student = Student.builder()
                .firstName("Hi!")
                .lastName("Bye")
                .email("email")
                .password("qwerty0")
                .build();

        System.out.println(student);
        // studentsRepository.update(student);
        // studentsRepository.delete(2L);
        System.out.println(studentsRepository.findAllByAgeGreaterThanOrderByIdDesc(20));

    }
}

