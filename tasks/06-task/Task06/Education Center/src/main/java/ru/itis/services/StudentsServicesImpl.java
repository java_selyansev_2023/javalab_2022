package ru.itis.services;

import ru.itis.dto.StudentSignUp;
import ru.itis.models.Student;
import ru.itis.repositories.StudentRepository;

public class StudentsServicesImpl implements StudentsServices {

    private final StudentRepository studentRepository;

    public StudentsServicesImpl(StudentRepository studentRepository) {
        this.studentRepository = studentRepository;
    }



    @Override
    public void signUp(StudentSignUp form) {

        Student student = Student.builder()
                .firstName(form.getFirstName())
                .lastName(form.getLastName())
                .email(form.getEmail())
                .password(form.getPassword())
                .build();


        studentRepository.save(student);

    }
}
